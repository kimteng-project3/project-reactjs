import React from "react";

function CardBlog({ card }) {
  return (
    <div className="card ">
      <div className="overflow-hidden">
        <img src={card.img} className="w-100 h20 img-card" alt={card.name} />
      </div>
      <span className="position-absolute  bg-primary text-white rounded-end mt-5 py-2 px-4">
        Web Design
      </span>
      <div
        className="card-body "
        style={{ backgroundColor: "var( --color-light-primary)" }}
      >
        <small className="me-3 container ">
          <i className="far fa-user text-primary me-2"></i>
          {card.name}
        </small>
        <small>
          <i className="far fa-calendar-alt text-primary me-2"></i>
          {card.date}
        </small>
        <h5 className="card-title">How to build a website</h5>
        <p className="card-text">{card.des}</p>
        <span className="text-uppercase">
          Read More <i className="fa-solid fa-arrow-right"></i>
        </span>
      </div>
    </div>
  );
}
export default CardBlog;
