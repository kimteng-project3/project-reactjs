import React, { useEffect } from "react";
import "../assets/css/main.css";
import AOS from "aos";
import "aos/dist/aos.css";
import FeaturePic from "../assets/image/feature.jpg";

const ChooseUsCom = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <div className="container margin-3rem">
      <div data-aos="fade-up" data-aos-duration="1000" data-aos-easing="linear">
        <div>
          <h5
            className=" fw-bold text-center"
            style={{ color: "var(--color-primary)" }}
          >
            WHY CHOOSE US
          </h5>

          <h1 className="about-text fw-bold w-50 mx-auto text-center ">
            We Are Here to Grow Your Business Exponentially
          </h1>

          <div className="loader mb-0 mx-auto"> </div>
        </div>
        <div className="row mt-5">
          <div className="col-lg-4 col-md-12">
            <div
              className="col-12 mb-5 pe-5"
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-easing="linear"
            >
              <i
                className="fa-solid fa-cubes p-3 icon mb-3"
                style={{ backgroundColor: "var(--color-primary)" }}
              ></i>
              <div>
                <h4 className="mb-3  fw-bold">Best In Industry</h4>
                <p style={{ color: "var(--color-lightwhite)" }}>
                  Magna sea eos sit dolor, ipsum amet lorem diam dolor eos et
                  diam dolor
                </p>
              </div>
            </div>
            <div
              className="col-12 pe-5"
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-easing="linear"
            >
              <i
                className="fa-solid fa-award p-3 icon mb-3 text-center"
                style={{ backgroundColor: "var(--color-primary)" }}
              ></i>
              <div>
                <h4 className="mb-3  fw-bold">Award Winning</h4>
                <p style={{ color: "var(--color-lightwhite)" }}>
                  Magna sea eos sit dolor, ipsum amet lorem diam dolor eos et
                  diam dolor
                </p>
              </div>
            </div>
          </div>
          <div
            className="col-lg-4 col-md-12 "
            data-aos="zoom-in"
            data-aos-duration="1000"
            data-aos-easing="linear"
          >
            <img src={FeaturePic} alt="Feature_Pic" className="img-fluid" />
          </div>
          <div className="col-lg-4 col-md-12 mt-lg-0 mt-md-5 mt-sm-5 mt-5 ">
            <div
              className="col-12 mb-5 ps-lg-5 ps-md-0"
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-easing="linear"
            >
              <i
                className="fa-solid fa-users p-3 icon mb-3 text-center"
                style={{ backgroundColor: "var(--color-primary)" }}
              ></i>
              <div>
                <h4 className="mb-3  fw-bold">Professional Staff</h4>
                <p style={{ color: "var(--color-lightwhite)" }}>
                  Magna sea eos sit dolor, ipsum amet lorem diam dolor eos et
                  diam dolor
                </p>
              </div>
            </div>
            <div
              className="col-12 mb-5 ps-lg-5 ps-md-0 ps-sm-0 ps-0"
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-easing="linear"
            >
              <i
                className="fa-solid fa-phone p-3 icon mb-3"
                style={{ backgroundColor: "var(--color-primary)" }}
              ></i>
              <div>
                <h4 className="mb-3  fw-bold">24/7 Support</h4>
                <p style={{ color: "var(--color-lightwhite)" }}>
                  Magna sea eos sit dolor, ipsum amet lorem diam dolor eos et
                  diam dolor
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ChooseUsCom;
