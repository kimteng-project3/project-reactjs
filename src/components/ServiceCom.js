import React, { useEffect } from "react";
import "../assets/css/main.css";
import AOS from "aos";
import "aos/dist/aos.css";

const ServiceCom = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <div className="container margin-3rem">
      <div data-aos="fade-up" data-aos-duration="1000" data-aos-easing="linear">
        <div>
          <h5
            className=" fw-bold text-center"
            style={{ color: "var(--color-primary)" }}
          >
            OUR SERVICES
          </h5>
          <div className="text-center">
            <h1 className="about-text  fw-bold w-50 mx-auto">
              Custom IT Solutions for Your Successful Business
            </h1>
          </div>
          <div className="loader mb-5 mx-auto"> </div>
          <div
            className="row"
            data-aos="fade-up"
            data-aos-duration="1000"
            data-aos-easing="linear"
          >
            <div className="col-lg-4 col-md-6 col-sm-12 col-12 mb-4 px-3">
              <div
                className="text-center p-5 boxs"
                style={{ backgroundColor: "var(--color-light-primary)" }}
              >
                <div className="mb-5 box-45deg mx-auto">
                  <i className="fa-solid fa-shield-halved text-white box-0deg p-3"></i>
                </div>
                <h4 className=" mb-3 fw-bold">Cyber Security</h4>
                <p>
                  Amet justo dolor lorem kasd amet magna sea stet eos vero lorem
                  ipsum dolore sed
                </p>
                <div className="text-center d-flex justify-content-center hover">
                  <i
                    className="fa-solid fa-arrow-right text-white p-3 rounded-1"
                    style={{ backgroundColor: "var(--color-primary)" }}
                  ></i>
                </div>
              </div>
            </div>
            <div
              className="col-lg-4 col-md-6 col-sm-12 col-12 mb-4 px-3"
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-easing="linear"
            >
              <div
                className="text-center p-5 boxs"
                style={{ backgroundColor: "var(--color-light-primary)" }}
              >
                <div className="mb-5 box-45deg mx-auto">
                  <i className="fa-solid fa-chart-pie text-white box-0deg p-3"></i>
                </div>
                <h4 className=" mb-4 fw-bold">Data Analytics</h4>
                <p>
                  Amet justo dolor lorem kasd amet magna sea stet eos vero lorem
                  ipsum dolore sed
                </p>
                <div className="text-center d-flex justify-content-center hover">
                  <i
                    className="fa-solid fa-arrow-right text-white p-3 rounded-1"
                    style={{ backgroundColor: "var(--color-primary)" }}
                  ></i>
                </div>
              </div>
            </div>
            <div
              className="col-lg-4 col-md-6 col-sm-12 col-12 mb-4 px-3"
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-easing="linear"
            >
              <div
                className="text-center p-5 boxs"
                style={{ backgroundColor: "var(--color-light-primary)" }}
              >
                <div className="mb-5 box-45deg mx-auto">
                  <i className="fa-solid fa-code text-white box-0deg p-3"></i>
                </div>
                <h4 className=" mb-3 fw-bold">Web Development</h4>
                <p>
                  Amet justo dolor lorem kasd amet magna sea stet eos vero lorem
                  ipsum dolore sed
                </p>
                <div className="text-center d-flex justify-content-center hover">
                  <i
                    className="fa-solid fa-arrow-right text-white p-3 rounded-1"
                    style={{ backgroundColor: "var(--color-primary)" }}
                  ></i>
                </div>
              </div>
            </div>
            <div
              className="col-lg-4 col-md-6 col-sm-12 col-12 mb-4 px-3"
              data-aos="zoom-in"
              data-aos-duration="1000"
            >
              <div
                className="text-center p-5 boxs"
                style={{ backgroundColor: "var(--color-light-primary)" }}
              >
                <div className="mb-5 box-45deg mx-auto">
                  <i className="fab fa-android text-white box-0deg p-3"></i>
                </div>
                <h4 className=" mb-3 fw-bold">Apps Development</h4>
                <p>
                  Amet justo dolor lorem kasd amet magna sea stet eos vero lorem
                  ipsum dolore sed
                </p>
                <div className="text-center d-flex justify-content-center hover">
                  <i
                    className="fa-solid fa-arrow-right text-white p-3 rounded-1"
                    style={{ backgroundColor: "var(--color-primary)" }}
                  ></i>
                </div>
              </div>
            </div>
            <div
              className="col-lg-4 col-md-6 col-sm-12 col-12 mb-4 px-3"
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-easing="linear"
            >
              <div
                className="text-center p-5 boxs"
                style={{ backgroundColor: "var(--color-light-primary)" }}
              >
                <div className="mb-5 box-45deg mx-auto">
                  <i className="fa-solid fa-search text-white box-0deg p-3"></i>
                </div>
                <h4 className=" mb-3 fw-bold">SEO Optimization</h4>
                <p>
                  Amet justo dolor lorem kasd amet magna sea stet eos vero lorem
                  ipsum dolore sed
                </p>
                <div className="text-center d-flex justify-content-center hover">
                  <i
                    className="fa-solid fa-arrow-right text-white p-3 rounded-1"
                    style={{ backgroundColor: "var(--color-primary)" }}
                  ></i>
                </div>
              </div>
            </div>
            <div
              className="col-lg-4 col-md-6 col-sm-12 col-12 mb-4 px-3"
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-easing="linear"
            >
              <div
                className="text-center p-5 boxs text-white"
                style={{ backgroundColor: "var(--color-primary)" }}
              >
                <h3 className=" mb-5 fw-bold">Call Us For Quote</h3>
                <p className="mb-5 text-white">
                  Clita ipsum magna kasd rebum at ipsum amet dolor justo dolor
                  est magna stet eirmod
                </p>
                <h2 className="fw-bold mb-3">+012 345 6789</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ServiceCom;
