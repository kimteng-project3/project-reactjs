import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import Image1 from "../assets/image/vendor-1.jpg";
import Image2 from "../assets/image/vendor-2.jpg";
import Image3 from "../assets/image/vendor-3.jpg";
import Image4 from "../assets/image/vendor-4.jpg";
import Image5 from "../assets/image/vendor-5.jpg";
import Image6 from "../assets/image/vendor-6.jpg";
import Image7 from "../assets/image/vendor-7.jpg";
import Image8 from "../assets/image/vendor-8.jpg";
import Image9 from "../assets/image/vendor-9.jpg";

const VendorCarousel = () => {
  const options = {
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    animateOut: true,
    animateIn: true,
    slidetransition: "linear",
    autoplayTimeout: 5000,
    autoplaySpeed: 1000,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 3,
        dots: false,
      },
      300: {
        items: 4,
        dots: false,
      },
      500: {
        items: 5,
        dots: false,
      },
      800: {
        items: 7,
        dots: false,
      },
      1200: {
        items: 8,
        dots: false,
      },
      1400: {
        items: 8,
        dots: false,
      },
    },
  };
  return (
    <div>
      <OwlCarousel
        className=" owl-theme margin-3rem d-flex justify-content-center container"
        {...options}
      >
        <div className="items">
          <img src={Image1} alt="Image_Vendor1" />
        </div>
        <div className="items">
          <img src={Image2} alt="Image_Vendor2" />
        </div>
        <div className="items">
          <img src={Image3} alt="Image_Vendor3" />
        </div>
        <div className="items">
          <img src={Image4} alt="Image_Vendor4" />
        </div>
        <div className="items">
          <img src={Image5} alt="Image_Vendor5" />
        </div>
        <div className="items">
          <img src={Image6} alt="Image_Vendor6" />
        </div>
        <div className="items">
          <img src={Image7} alt="Image_Vendor7" />
        </div>
        <div className="items">
          <img src={Image8} alt="Image_Vendor8" />
        </div>
        <div className="items">
          <img src={Image9} alt="Image_Vendor9" />
        </div>
      </OwlCarousel>
    </div>
  );
};

export default VendorCarousel;
