import React, { useEffect } from "react";
import "../assets/css/main.css";
import AOS from "aos";
import "aos/dist/aos.css";

const PricingPlanCom = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <div className="container margin-3rem">
      <div data-aos="fade-up" data-aos-duration="1000" data-aos-easing="linear">
        <h5
          className=" fw-bold text-center"
          style={{ color: "var(--color-primary)" }}
        >
          PRICING PLANS
        </h5>
        <div className="text-center">
          <h1 className="about-text  fw-bold w-50 mx-auto">
            We are Offering Competitive Prices for Our Clients
          </h1>
        </div>
        <div className="loader mb-5 mx-auto"> </div>
        <div className="row">
          <div
            className="col-lg-4 col-md-12 p-0 rounded"
            data-aos="zoom-in"
            data-aos-duration="900"
            data-aos-easing="linear"
          >
            <div
              className="p-5"
              style={{ backgroundColor: "var(--color-light-primary)" }}
            >
              <h4 className="fw-bold" style={{ color: "var(--color-primary)" }}>
                Basic Plan
              </h4>
              <p>FOR SMALL SIZE BUSINESS</p>
              <hr />
              <div>
                <div className="d-flex fw-bold">
                  <small>$</small>
                  <h1 style={{ color: "var(--color-light-blue)" }}>
                    49.00
                    <small className="fs-6">/Month</small>
                  </h1>
                </div>
                <div className="d-flex justify-content-between">
                  <p>HTML5 & CSS3</p>
                  <span>
                    <i
                      className="fa-solid fa-check fw-bold"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Bootstrap v5</p>
                  <span>
                    <i
                      className="fa-solid fa-check"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Responsive Layout</p>
                  <span>
                    <i
                      className="fa-solid fa-xmark"
                      style={{ color: "var(--color-red)" }}
                    ></i>
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Cross-browser Support</p>
                  <span>
                    <i
                      className="fa-solid fa-xmark"
                      style={{ color: "var(--color-red)" }}
                    ></i>
                  </span>
                </div>
                <button
                  className="border-0 px-5 py-3 text-white"
                  style={{ backgroundColor: "var(--color-primary)" }}
                >
                  Order Now
                </button>
              </div>
            </div>
          </div>
          <div
            className="col-lg-4 col-md-12 p-0 shadow rounded"
            data-aos="zoom-in"
            data-aos-duration="800"
            data-aos-easing="linear"
          >
            <div
              className="p-5"
              style={{
                backgroundColor: "var(--color-white)",
              }}
            >
              <h4 className="fw-bold" style={{ color: "var(--color-primary)" }}>
                Standard Plan
              </h4>
              <p>For Medium Size Business</p>
              <hr />
              <div>
                <div className="d-flex fw-bold">
                  <small>$</small>
                  <h1 style={{ color: "var(--color-light-blue)" }}>
                    99.00
                    <small className="fs-6">/Month</small>
                  </h1>
                </div>
                <div className="d-flex justify-content-between">
                  <p>HTML5 & CSS3</p>
                  <span>
                    <i
                      className="fa-solid fa-check fw-bold"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Bootstrap v5</p>
                  <span>
                    <i
                      className="fa-solid fa-check"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Responsive Layout</p>
                  <span>
                    <i
                      className="fa-solid fa-check"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Cross-browser Support</p>
                  <span>
                    <i
                      className="fa-solid fa-xmark"
                      style={{ color: "var(--color-red)" }}
                    ></i>
                  </span>
                </div>
                <button
                  className="border-0 px-5 py-3 text-white"
                  style={{ backgroundColor: "var(--color-primary)" }}
                >
                  Order Now
                </button>
              </div>
            </div>
          </div>
          <div
            className="col-lg-4 col-md-12 p-0 rounded"
            data-aos="zoom-in"
            data-aos-duration="1000"
            data-aos-easing="linear"
          >
            <div
              className="p-5"
              style={{ backgroundColor: "var(--color-light-primary)" }}
            >
              <h4 className="fw-bold" style={{ color: "var(--color-primary)" }}>
                Advanced Plan
              </h4>
              <p>For Large Size Business</p>
              <hr />
              <div>
                <div className="d-flex fw-bold">
                  <small>$</small>
                  <h1 style={{ color: "var(--color-light-blue)" }}>
                    149.00
                    <small className="fs-6">/Month</small>
                  </h1>
                </div>
                <div className="d-flex justify-content-between">
                  <p>HTML5 & CSS3</p>
                  <span>
                    <i
                      className="fa-solid fa-check fw-bold"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Bootstrap v5</p>
                  <span>
                    <i
                      className="fa-solid fa-check"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Responsive Layout</p>
                  <span>
                    <i
                      className="fa-solid fa-check"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Cross-browser Support</p>
                  <span>
                    <i
                      className="fa-solid fa-check"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                  </span>
                </div>
                <button
                  className="border-0 px-5 py-3 text-white"
                  style={{ backgroundColor: "var(--color-primary)" }}
                >
                  Order Now
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PricingPlanCom;
