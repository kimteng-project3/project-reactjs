import React, { useEffect } from "react";
import "../assets/css/main.css";
import AOS from "aos";
import "aos/dist/aos.css";

const ContactCom = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <div className="container margin-3rem">
      <div data-aos="fade-up" data-aos-duration="1000" data-aos-easing="linear">
        <h5
          className=" fw-bold text-center"
          style={{ color: "var(--color-primary)" }}
        >
          CONTACT US
        </h5>
        <div className="text-center">
          <h1 className="about-text  fw-bold w-50 mx-auto">
            If You Have Any Query, Feel Free To Contact Us
          </h1>
        </div>
        <div className="loader mb-5 mx-auto"> </div>
        <div className="row">
          <div className="col-lg-4 col-md-12">
            <div className=" d-flex  mt-4">
              <div
                className="icon fs-5 phone text-center me-4 mt-2"
                style={{ backgroundColor: "var(--color-primary)" }}
              >
                <i className="fa-solid fa-phone"></i>
              </div>
              <div>
                <div>Call to ask any question</div>
                <div
                  className="fs-5 fw-bold"
                  style={{ color: "var(--color-primary)" }}
                >
                  +012 345 6789
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-12">
            <div className=" d-flex  mt-4">
              <div
                className="icon fs-5 phone text-center me-4 mt-2"
                style={{ backgroundColor: "var(--color-primary)" }}
              >
                <i className="fa-solid fa-envelope"></i>
              </div>
              <div>
                <div>Email to get free quote</div>
                <div
                  className="fs-5 fw-bold"
                  style={{ color: "var(--color-primary)" }}
                >
                  info@example.com
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-12">
            <div className=" d-flex  mt-4">
              <div
                className="icon fs-5 phone text-center me-4 mt-2"
                style={{ backgroundColor: "var(--color-primary)" }}
              >
                <i className="fa-solid fa-location"></i>
              </div>
              <div>
                <div>Visit our office</div>
                <div
                  className="fs-5 fw-bold"
                  style={{ color: "var(--color-primary)" }}
                >
                  123 Street, NY, USA
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-5">
          <div className="col-lg-6 col-md-12 margin-3rem">
            <form action="#">
              <div className="row">
                <div className="col-6">
                  <input
                    type="text"
                    placeholder="Your Name"
                    className="form-control"
                    style={{ backgroundColor: "var(--color-light-primary)" }}
                  />
                </div>
                <div className="col-6">
                  <input
                    type="email"
                    placeholder="Your Email"
                    className="form-control"
                    style={{ backgroundColor: "var(--color-light-primary)" }}
                  />
                </div>
                <div className="col-12 mt-3">
                  <input
                    type="text"
                    placeholder="Subject"
                    className="form-control"
                    style={{ backgroundColor: "var(--color-light-primary)" }}
                  />
                </div>
                <div className="col-12 mt-3">
                  <textarea
                    name=""
                    id=""
                    cols="30"
                    rows="6"
                    className="form-control"
                    style={{ backgroundColor: "var(--color-light-primary)" }}
                    placeholder="Message"
                  ></textarea>
                </div>
                <div className="col-12">
                  <button
                    className="border-0 text-white p-2 w-100 mt-3"
                    style={{ backgroundColor: "var(--color-primary)" }}
                  >
                    Send Message
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div className="col-lg-6 col-md-12 mt-lg-0 mt-md-3 mt-sm-3 mt-3">
            <iframe
              title="Google Maps"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15636.10295332813!2d104.92919123440282!3d11.550011342495493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310956d330da8527%3A0x7eff3ca8d12ecc0f!2z4Z6A4Z-E4Z-H4Z6W4Z-B4Z6H4Z-S4Z6a!5e0!3m2!1skm!2skh!4v1707026179299!5m2!1skm!2skh"
              width="100%"
              height="405"
              style={{ border: 0 }}
              allowFullScreen=""
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
            ></iframe>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactCom;
