import React from "react";
import VendorCarousel from "../../../components/VendorCarousel";
import Footer from "../../layouts/footer/Footer";
import BackToTopCom from "../../../components/BackToTopCom";
import Image1 from "../../../assets/image/blog-1.jpg";
import Image2 from "../../../assets/image/blog-2.jpg";
import Image3 from "../../../assets/image/blog-3.jpg";
import Image_Me from "../../../assets/image/testmonial4.jpg";

const BlogDetailPage = () => {
  const handleClick = () => {
    window.scrollTo(0, 0);
  };
  return (
    <div>
      <div className="container margin-3rem">
        <div className="row">
          <div className="col-lg-8 col-md-12 mb-5">
            <div>
              <div>
                <img src={Image1} alt="Image_one" className="img-fluid w-100" />
                <h1 className="mt-5 mb-5">
                  Diam dolor est labore duo ipsum clita sed et lorem tempor duo
                </h1>
                <p>
                  Sadipscing labore amet rebum est et justo gubergren. Et eirmod
                  ipsum sit diam ut magna lorem. Nonumy vero labore lorem
                  sanctus rebum et lorem magna kasd, stet amet magna accusam
                  consetetur eirmod. Kasd accusam sit ipsum sadipscing et at at
                  sanctus et. Ipsum sit gubergren dolores et, consetetur justo
                  invidunt at et aliquyam ut et vero clita. Diam sea sea no sed
                  dolores diam nonumy, gubergren sit stet no diam kasd vero.
                </p>
                <p>
                  Voluptua est takimata stet invidunt sed rebum nonumy stet,
                  clita aliquyam dolores vero stet consetetur elitr takimata
                  rebum sanctus. Sit sed accusam stet sit nonumy kasd diam
                  dolores, sanctus lorem kasd duo dolor dolor vero sit et.
                  Labore ipsum duo sanctus amet eos et. Consetetur no sed et
                  aliquyam ipsum justo et, clita lorem sit vero amet amet est
                  dolor elitr, stet et no diam sit. Dolor erat justo dolore sit
                  invidunt.
                </p>
                <p>
                  Diam dolor est labore duo invidunt ipsum clita et, sed et
                  lorem voluptua tempor invidunt at est sanctus sanctus. Clita
                  dolores sit kasd diam takimata justo diam lorem sed. Magna
                  amet sed rebum eos. Clita no magna no dolor erat diam tempor
                  rebum consetetur, sanctus labore sed nonumy diam lorem amet
                  eirmod. No at tempor sea diam kasd, takimata ea nonumy elitr
                  sadipscing gubergren erat. Gubergren at lorem invidunt
                  sadipscing rebum sit amet ut ut, voluptua diam dolores at
                  sadipscing stet. Clita dolor amet dolor ipsum vero ea ea eos.
                </p>
                <p>
                  Voluptua est takimata stet invidunt sed rebum nonumy stet,
                  clita aliquyam dolores vero stet consetetur elitr takimata
                  rebum sanctus. Sit sed accusam stet sit nonumy kasd diam
                  dolores, sanctus lorem kasd duo dolor dolor vero sit et.
                  Labore ipsum duo sanctus amet eos et. Consetetur no sed et
                  aliquyam ipsum justo et, clita lorem sit vero amet amet est
                  dolor elitr, stet et no diam sit. Dolor erat justo dolore sit
                  invidunt.
                </p>
              </div>
              <div>
                <h3
                  className="mt-5 fw-bold"
                  style={{ color: "var(--color-darkgreen)" }}
                >
                  3 Comments
                </h3>
                <div className="loader"></div>
                <div className="d-flex justify-content-between mt-5">
                  <div>
                    <img
                      src={Image_Me}
                      alt="Image_ME"
                      style={{ width: "45px", height: "45px" }}
                    />
                  </div>
                  <div className="px-3">
                    <span style={{ color: "var(--color-primary)" }}>
                      Kim Hak
                    </span>{" "}
                    <span className="fst-italic">01 Jan 2024</span>
                    <p>
                      Diam amet duo labore stet elitr invidunt ea clita ipsum
                      voluptua, tempor labore accusam ipsum et no at. Kasd diam
                      tempor rebum magna dolores sed eirmod
                    </p>
                    <button
                      className="border-0 px-3 py-1"
                      style={{ backgroundColor: "var(--color-light-primary)" }}
                    >
                      Reply
                    </button>
                  </div>
                </div>
                <div className="d-flex justify-content-between mt-5">
                  <div>
                    <img
                      src={Image_Me}
                      alt="Image_ME"
                      style={{ width: "45px", height: "45px" }}
                    />
                  </div>
                  <div className="px-3">
                    <span style={{ color: "var(--color-primary)" }}>
                      Kim Hak
                    </span>{" "}
                    <span className="fst-italic">01 Jan 2024</span>
                    <p>
                      Diam amet duo labore stet elitr invidunt ea clita ipsum
                      voluptua, tempor labore accusam ipsum et no at. Kasd diam
                      tempor rebum magna dolores sed eirmod
                    </p>
                    <button
                      className="border-0 px-3 py-1"
                      style={{ backgroundColor: "var(--color-light-primary)" }}
                    >
                      Reply
                    </button>
                  </div>
                </div>
                <div className="d-flex justify-content-between mt-5 px-5">
                  <div>
                    <img
                      src={Image_Me}
                      alt="Image_ME"
                      style={{ width: "45px", height: "45px" }}
                    />
                  </div>
                  <div className="px-3">
                    <span style={{ color: "var(--color-primary)" }}>
                      Kim Hak
                    </span>{" "}
                    <span className="fst-italic">01 Jan 2024</span>
                    <p>
                      Diam amet duo labore stet elitr invidunt ea clita ipsum
                      voluptua, tempor labore accusam ipsum et no at. Kasd diam
                      tempor rebum magna dolores sed eirmod
                    </p>
                    <button
                      className="border-0 px-3 py-1"
                      style={{ backgroundColor: "var(--color-light-primary)" }}
                    >
                      Reply
                    </button>
                  </div>
                </div>
              </div>
              <form
                className="p-5 mt-5"
                style={{ backgroundColor: "var(--color-light-primary)" }}
              >
                <h3 className="fw-bold">Leave A Comment</h3>
                <div className="loader"></div>
                <div className="row mt-3">
                  <div className="col-md-6 col-sm-12">
                    <input
                      type="text"
                      placeholder="Your Name"
                      className="form-control mb-2"
                    />
                  </div>
                  <div className="col-md-6 col-sm-12">
                    <input
                      type="email"
                      placeholder="Your Email"
                      className="form-control mb-2"
                    />
                  </div>
                  <div className="col-12">
                    <input
                      type="url"
                      placeholder="Website"
                      className="form-control mb-2"
                    />
                  </div>
                  <div className="col-12">
                    <textarea
                      name=""
                      id=""
                      cols="30"
                      rows="10"
                      placeholder="Comment"
                      className="form-control mb-3"
                    ></textarea>
                  </div>
                  <div className="col-12">
                    <button
                      className="w-100 border-0 p-2 text-white"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Leave Your Comment
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div className="col-lg-4 col-md-12">
            <div className="row">
              <div className="col-lg-9 col-md-9 col-sm-9 col-9 p-0">
                <input
                  type="text"
                  className="form-control rounded-end-0  rounded-start-0 p-3"
                  placeholder="Keyword"
                />
              </div>
              <div className="col-lg-3 col-md-3 col-sm-3 col-3 p-0">
                <button
                  className="form-control rounded-start-0 rounded-end-0 p-3"
                  style={{ color: "var(--color-primary)" }}
                >
                  <i className="fa-solid fa-search"></i>
                </button>
              </div>
              <div className="col-lg-3 col-md-12">
                <h3 className="mt-5 fw-bold">Categories</h3>
                <div className="loader  mb-3"></div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Web Design
                  </a>
                </div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Web Development
                  </a>
                </div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Web Development
                  </a>
                </div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Keyword Research
                  </a>
                </div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Email Marketing
                  </a>
                </div>
              </div>
              <div className="col-12">
                <h3 className="mt-5 fw-bold">Recent Post</h3>
                <div className="loader  mb-3"></div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image1}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image2}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image3}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image1}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image2}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image3}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 mt-4">
                <img
                  src={Image1}
                  alt="Image_one"
                  className=" img-fluid w-100"
                />
              </div>
              <div className="col-12">
                <h3 className="mt-5 fw-bold">Tag Cloud</h3>
                <div className="loader  mb-3"></div>
              </div>
              <div className="col-12 d-flex flex-wrap">
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Design
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Development
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Marketing
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  SEO
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Writing
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Consulting
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Design
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Development
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Marketing
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  SEO
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Writng
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Consulting
                </div>
              </div>
              <div className="col-12 ">
                <h3 className="mt-5 fw-bold">Plain Text</h3>
                <div className="loader  mb-3"></div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3 text-center"
                >
                  <p>
                    Vero sea et accusam justo dolor accusam lorem consetetur,
                    dolores sit amet sit dolor clita kasd justo, diam accusam no
                    sea ut tempor magna takimata, amet sit et diam dolor ipsum
                    amet diam
                  </p>
                  <button
                    className="border-0 px-4 py-2 text-white mt-1 mb-3"
                    style={{ backgroundColor: "var(--color-primary)" }}
                  >
                    Read More
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <VendorCarousel />
      <Footer />
      <BackToTopCom />
    </div>
  );
};

export default BlogDetailPage;
