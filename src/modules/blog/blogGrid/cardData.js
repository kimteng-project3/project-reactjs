export const card = [
  {
    id: 1,
    title: "How to build a website",
    name: "John Doe",
    date: "01 Jan, 2045",
    des: "Dolor et eos labore stet justo sed est sed sed sed dolor stet amet",
    img: "http://kiloit-learning.s3-website-ap-southeast-1.amazonaws.com/reactjs/team-1/sample-to-react/img/blog-1.jpg",
  },
  {
    id: 2,
    title: "How to build a website",
    name: "John Doe",
    date: "01 Jan, 2045",
    des: "Dolor et eos labore stet justo sed est sed sed sed dolor stet amet",
    img: "http://kiloit-learning.s3-website-ap-southeast-1.amazonaws.com/reactjs/team-1/sample-to-react/img/blog-2.jpg",
  },
  {
    id: 3,
    title: "How to build a website",
    name: "John Doe",
    date: "01 Jan, 2045",
    des: "Dolor et eos labore stet justo sed est sed sed sed dolor stet amet",
    img: "http://kiloit-learning.s3-website-ap-southeast-1.amazonaws.com/reactjs/team-1/sample-to-react/img/blog-3.jpg",
  },
];
