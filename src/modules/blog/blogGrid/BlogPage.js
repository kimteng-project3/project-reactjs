import CardBlog from "../../../components/CardBlog";
import { card } from "./cardData";

const BlogPage = () => {
  return (
    <div className="container p-5 margin-3rem">
      <h5
        className=" fw-bold text-center"
        style={{ color: "var(--color-primary)" }}
      >
        Latest Blog
      </h5>
      <div className="text-center">
        <h1 className="about-text  fw-bold w-50 mx-auto">
          Read The Latest Articles from Our Blog Post
        </h1>
        <div className="loader mb-5 mx-auto"> </div>
      </div>

      <div className="row p-5">
        {card.map((cardItem, id) => (
          <div className="col-lg-4 col-md-12 col-sm-12 " key={id}>
            <CardBlog card={cardItem} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default BlogPage;
