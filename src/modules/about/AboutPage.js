import React from "react";
import AboutUseCom from "../../components/AboutUseCom";
import VendorCarousel from "../../components/VendorCarousel";
import Footer from "../layouts/footer/Footer";
import BackToTopCom from "../../components/BackToTopCom";

const AboutPage = () => {
  return (
    <div>
      <AboutUseCom />
      <VendorCarousel />
      <BackToTopCom/>
      <Footer/>
    </div>
  );
};

export default AboutPage;
