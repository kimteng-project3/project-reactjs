import React from "react";
import ChooseUsCom from "../../components/ChooseUsCom";
import VendorCarousel from "../../components/VendorCarousel";
import Footer from "../layouts/footer/Footer";
import BackToTopCom from "../../components/BackToTopCom";

const OurFeaturesPage = () => {
  return (
    <div>
      <ChooseUsCom/>
      <VendorCarousel/>
      <Footer/>
      <BackToTopCom/>
    </div>
  );
};

export default OurFeaturesPage;
