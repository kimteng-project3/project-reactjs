import React from "react";
import VendorCarousel from "../../components/VendorCarousel";
import Footer from "../layouts/footer/Footer";
import BackToTopCom from "../../components/BackToTopCom";

const FreeQuotePage = () => {
  return (
    <div>
      <VendorCarousel/>
      <Footer/>
      <BackToTopCom/>
    </div>
  );
};

export default FreeQuotePage;
