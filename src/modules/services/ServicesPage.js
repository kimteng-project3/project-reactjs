import React from "react";
import ServiceCom from "../../components/ServiceCom";
import Testmonial from "../../components/TestmonialCom";
import VendorCarousel from "../../components/VendorCarousel";
import Footer from "../layouts/footer/Footer";
import BackToTopCom from "../../components/BackToTopCom";

const ServicesPage = () => {
  return (
    <div>
      <ServiceCom/>
      <Testmonial/>
      <VendorCarousel/>
      <Footer/>
      <BackToTopCom/>
    </div>
  );
};

export default ServicesPage;
