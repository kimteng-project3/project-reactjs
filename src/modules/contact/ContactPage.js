import React from "react";
import VendorCarousel from "../../components/VendorCarousel";
import Footer from "../layouts/footer/Footer";
import BackToTopCom from "../../components/BackToTopCom";
import ContactCom from "../../components/ContactCom";

const ContactPage = () => {
  return (
    <div>
      <ContactCom/>
      <VendorCarousel/>
      <Footer/>
      <BackToTopCom/>
    </div>
  );
};

export default ContactPage;
