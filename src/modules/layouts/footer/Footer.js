import React from "react";

const Footer = () => {
  return (
    <>
      <div
        className="container-fluid margin-3rem"
        style={{ backgroundColor: "var(--color-darkgreen)" }}
      >
        <div className="container">
          <div className="row">
            <div
              className="col-xl-4 col-lg-4 col-md-12 p-4 text-white footer"
              style={{
                backgroundColor: "var(--color-primary)",
              }}
            >
              <h1 className="fw-bold">
                <i className="fa-solid fa-user"></i> <span>Startup</span>
              </h1>
              <p className="text-white">
                Lorem diam sit erat dolor elitr et, diam lorem justo amet clita
                stet eos sit. Elitr dolor duo lorem, elitr clita ipsum sea. Diam
                amet erat lorem stet eos. Diam amet et kasd eos duo.
              </p>
              <form>
                <input type="email" placeholder="Your Email" />
                <input type="submit" value="Sign Up" placeholder="Sign Up" />
              </form>
            </div>
            <div className="col-8">
              <div className="row">
                <div className="col-lg-4 col-md-12 p-3 pt-lg-3 pt-md-5 text-white">
                  <h3>Get In Touch</h3>
                  <div className="loader bg-white"></div>
                  <div>
                    <i
                      className="fa-solid fa-location-dot mt-4"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                    <span className="ms-2">123 Street, New York, USA</span>
                  </div>
                  <div>
                    <i
                      className="fa-solid fa-envelope-open mt-4"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                    <span className="ms-2">info@example.com</span>
                  </div>
                  <div>
                    <i
                      className="fa-solid fa-phone mt-4"
                      style={{ color: "var(--color-primary)" }}
                    ></i>
                    <span className="ms-2">+012 345 67890</span>
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 p-3 text-white">
                  <h3>Quick Links</h3>
                  <div className="loader bg-white"></div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Home</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">About Us</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Our Services</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Meet The Team</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Latest Blog</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Contact Us</span>
                    </a>
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 p-3 text-white">
                  <h3>Popular Links</h3>
                  <div className="loader bg-white"></div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Home</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">About Us</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Our Services</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Meet The Team</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Latest Blog</span>
                    </a>
                  </div>
                  <div className="mt-3 menu-footer">
                    <a
                      href="#noPage"
                      onClick={() => window.scrollTo(0, 0)}
                      className="text-decoration-none"
                    >
                      <i
                        className="fa-solid fa-arrow-right"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span className="text-white ms-2">Contact Us</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="container-fluid text-white"
        style={{ backgroundColor: "var(--color-darkblue)" }}
      >
        <div className="container d-lg-flex d-md-block justify-content-center p-3 text-center">
          <p className="text-white mt-2">
            &copy;{" "}
            <a href="#noPage" onClick={() => window.scrollTo(0, 0)}>
              TeamOne.
            </a>{" "}
            All Rights Reserved. Designed by TeamOne
          </p>
          <p className="mt-2 ms-4">
            Distributed By: <a href="https://themewagon.com/">ThemeWagon</a>
          </p>
        </div>
      </div>
    </>
  );
};

export default Footer;
