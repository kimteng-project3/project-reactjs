import React from "react";
import Index from "../Index";
import { Outlet } from "react-router-dom";
import Footer from "../footer/Footer";

const Layout = () => {
  return (
    <div>
      <Index />
      <Footer />
      <Outlet />
    </div>
  );
};

export default Layout;
