import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "../src/modules/home/HomePage";
import AboutPage from "../src/modules/about/AboutPage";
import ServicesPage from "../src/modules/services/ServicesPage";
import BlogGridPage from "../src/modules/blog/blogGrid/BlogGridPage";
import BlogDetailPage from "../src/modules/blog/blogDetail/BlogDetailPage";
import PricingPlanPage from "../src/modules/pages/PricingPlanPage";
import OurFeaturesPage from "../src/modules/pages/OurFeaturesPage";
import TeamMemberPage from "../src/modules/pages/TeamMemberPage";
import TestimonialPage from "../src/modules/pages//TestimonialPage";
import FreeQuotePage from "../src/modules/pages/FreeQuotePage";
import ContactPage from "../src/modules/contact/ContactPage";
import ErrorPage from "../src/modules/Error/ErrorPage";
import Layout from "./modules/layouts/header/Layout";
import TopBarCom from "./components/TopBarCom";

const NavBarCom = () => {
  return (
    <>
    <TopBarCom/>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index path="" element={<HomePage />} />
          <Route path="about" element={<AboutPage />} />
          <Route path="services" element={<ServicesPage />} />
          <Route path="blogGrid" element={<BlogGridPage />} />
          <Route path="blogDetail" element={<BlogDetailPage />} />
          <Route path="pricingPlan" element={<PricingPlanPage />} />
          <Route path="ourFeatures" element={<OurFeaturesPage />} />
          <Route path="teamMembers" element={<TeamMemberPage />} />
          <Route path="tesimonial" element={<TestimonialPage />} />
          <Route path="freeQuote" element={<FreeQuotePage />} />
          <Route path="contact" element={<ContactPage />} />
          <Route path="*" element={<ErrorPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
    </>
  );
};


export default NavBarCom;
